<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190911080617 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE access_token CHANGE scope scope VARCHAR(191) DEFAULT NULL');
        $this->addSql('ALTER TABLE refresh_token CHANGE scope scope VARCHAR(191) DEFAULT NULL');
        $this->addSql('ALTER TABLE user_list DROP FOREIGN KEY FK_3E49B4D1A76ED395');
        $this->addSql('DROP INDEX IDX_3E49B4D1A76ED395 ON user_list');
        $this->addSql('ALTER TABLE user_list CHANGE user_id user INT NOT NULL');
        $this->addSql('ALTER TABLE user_list ADD CONSTRAINT FK_3E49B4D18D93D649 FOREIGN KEY (user) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_3E49B4D18D93D649 ON user_list (user)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE access_token CHANGE scope scope VARCHAR(191) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE refresh_token CHANGE scope scope VARCHAR(191) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE user_list DROP FOREIGN KEY FK_3E49B4D18D93D649');
        $this->addSql('DROP INDEX IDX_3E49B4D18D93D649 ON user_list');
        $this->addSql('ALTER TABLE user_list CHANGE user user_id INT NOT NULL');
        $this->addSql('ALTER TABLE user_list ADD CONSTRAINT FK_3E49B4D1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_3E49B4D1A76ED395 ON user_list (user_id)');
    }
}
