<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;


/**
 * Default route for Movie Controller
 */
abstract class BaseController extends AbstractFOSRestController
{

    /**
     * @var EntityManagerInterface $em
     */
    protected $em;

    /**
     * @var array $groups
     */
    protected $groups = ['always'];

    /**
     * BaseController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * A default groups directly set inside the child controller
     *
     * @return array
     */
    protected function defaultGroups(): array
    {
        return [];
    }

    /**
     * @param array $groups
     * @return BaseController
     */
    public function addGroups(array $groups): self
    {
        $this->groups = array_merge($this->groups, $groups);

        return $this;
    }

    /**
     * @param string $group
     * @return BaseController
     */
    public function setGroup(string $group): self
    {
        if (!in_array($group, $this->groups)) {
            $this->groups[] = $group;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @param array|object $entity
     */
    public function persistFlush($entity)
    {
        if (is_array($entity)) {
            foreach ($entity as $oneEntity) {
                $this->em->persist($oneEntity);
            }
        } else {
            $this->em->persist($entity);
        }

        $this->em->flush();
    }

    /**
     * @param array|object $entity
     */
    public function removeAndFlush($entity)
    {
        if (is_array($entity)) {
            foreach ($entity as $oneEntity) {
                $this->em->remove($oneEntity);
            }
        } else {
            $this->em->remove($entity);
        }

        $this->em->flush();
    }
}