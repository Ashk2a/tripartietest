<?php

namespace App\Controller;

use App\Service\ParamHandler;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations\ParamInterface;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/** @QueryParam(
 *     name="format",
 *     requirements="(json|xml)",
 *     allowBlank=true,
 *     default="json",
 *     description="Format API response"
 *  )
 * @QueryParam(
 *     name="order",
 *     allowBlank=true,
 *     default="id, desc",
 *     description="Sort SQL Result"
 *  )
 * @QueryParam(
 *     name="fields",
 *     allowBlank=true,
 *     default="*",
 *     description="Select fields"
 *  )
 *
 */
class BaseApiController extends BaseController
{
    public const NOT_FOUND = 'Resource not found';
    public const ACCESS_DENIED = 'You are not allowed to do that!';
    public const BAD_REQUEST = 'Bad request my friend..';


    /**
     * @var ParamHandler $paramHandler ;
     */
    private $paramHandler;

    /**
     * BaseApiController constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ParamFetcherInterface $paramFetcher
     * @param ParamHandler $paramHandler
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ParamFetcherInterface $paramFetcher,
        ParamHandler $paramHandler
    )
    {
        parent::__construct($entityManager);
        $this->paramHandler = $paramHandler;
        $this->paramHandler
            ->setParamFetcher($paramFetcher)
            // This is the conf to make an inheritance between THIS class and their child
            // You can do $this->paramHandler->setParentsClasses(parent::class) in child class too!
            // You can inherit ParamFetcher annotions from another class !
            ->setParentsClasses([self::class])
            ->setGroups($this->getGroups());
    }

    /**
     * Build FormInterface with given $type
     *
     * @param string $type
     * @param null $entity
     * @param array $options
     * @return FormInterface
     */
    public function getForm(string $type, $entity = null, $options = []): FormInterface
    {
        $defaultGroupValidator = ['validation_groups' => ['Default']];
        $form = $this->createForm($type, $entity, array_merge($options, $defaultGroupValidator));
        return $form;
    }

    /**
     * Return a ParamInterface by his name attribute
     *
     * @param string $name
     * @return array
     */
    public function getParam(string $name)
    {
        return $this->paramHandler->getParam($name);
    }

    /**
     * Get all params with details
     *
     * @param bool $details
     * @return ParamInterface[]|array
     */
    public function getParams(bool $details = false)
    {
        if ($details) {
            return $this->paramHandler->getParams();
        }
        return $this->paramHandler->getAll();
    }

    /**
     * @return ParamHandler
     */
    public function getParamHandler()
    {
        return $this->paramHandler;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasParam(string $name)
    {
        return $this->paramHandler->hasParam($name);
    }

    /**
     * Creates a view.
     *
     * Convenience method to allow for a fluent interface.
     *
     * @param mixed $object
     * @param int $statusCode
     * @param array $headers
     *
     * @return View
     */
    public function view($object = null, $statusCode = 200, array $headers = []): View
    {
        $format = $this->getParam('format');

        $this->groups = array_merge($this->defaultGroups(), $this->groups);

        $context = new Context();
        $context->setGroups($this->groups);

        $view = parent::view($object, $statusCode, $headers);
        $view->setContext($context);
        $view->setHeader('Content-Type', 'application/' . $format);
        $view->setFormat($format);

        return $view;
    }

    /**
     * @param null $object
     * @return Response
     */
    public function resourceCreateResponse($object = null): Response
    {
        return $this->handleView(
            $this->view($object, Response::HTTP_CREATED)
        );
    }

    /**
     * @param null $object
     * @return Response
     */
    public function resourceOkResponse($object = null): Response
    {
        return $this->handleView(
            $this->view($object, Response::HTTP_OK)
        );
    }

    /**
     * @param null $object
     * @return Response
     */
    public function resourceDeleteResponse($object = null): Response
    {
        return $this->handleView(
            $this->view($object, Response::HTTP_OK)
        );
    }

    /**
     * @return array
     */
    private function getExceptionHeaders(): array
    {
        $format = (
        (!$this->hasParam('format'))
            ? 'json'
            : $this->getParam('format')
        );
        return ['Content-Type' => 'application/' . $format];
    }


    /**
     * @param string|null $message
     * @return NotFoundHttpException
     */
    public function notFoundException(?string $message = self::NOT_FOUND): NotFoundHttpException
    {
        return new NotFoundHttpException($message, null, 0, $this->getExceptionHeaders());
    }

    /**
     * @param string|null $message
     * @return BadRequestHttpException
     */
    public function badRequestException(?string $message = self::BAD_REQUEST)
    {
        return new BadRequestHttpException($message, null, 0, $this->getExceptionHeaders());
    }

    /**
     * @param string|null $message
     * @return AccessDeniedHttpException
     */
    public function accessDeniedException(?string $message = self::ACCESS_DENIED): AccessDeniedHttpException
    {
        return new AccessDeniedHttpException($message, null, 0, $this->getExceptionHeaders());
    }

    /**
     * @param string|null $message
     * @return ConflictHttpException
     */
    public function conflictException(?string $message)
    {
        return new ConflictHttpException($message, null, 0, $this->getExceptionHeaders());
    }
}