<?php

namespace App\Controller\ApiAdmin;

use App\Controller\BaseApiController;
use App\Entity\Movie;
use App\Form\MovieType;
use App\Service\FormHandler;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class MovieController extends BaseApiController
{
    protected function defaultGroups(): array
    {
        return ['admin'];
    }

    /**
     * Create a movie
     *
     * @param FormHandler $formHandler
     * @return Response
     */
    public function create(FormHandler $formHandler): Response
    {
        $movie = new Movie();

        try {
            $form = $this->getForm(MovieType::class, $movie);
            if (!$formHandler->process($form)) {
                throw $this->badRequestException($formHandler->error());
            }

            $this->persistFlush($movie);
        } catch (\Exception $e) {
            throw $this->badRequestException($e->getMessage());
        }

        return $this->handleView($this->view($movie));
    }

    public function createFromIMDb(Request $request, FormHandler $formHandler) : Response {
        $url = 'https://www.omdbapi.com/';
        $id = $request->get('IMDbId');
        $apiKey = 'fd606d1b';

        $client = HttpClient::create();
        try {
            $response = $client->request('GET', sprintf('%s?i=%s&apikey=%s', $url, $id, $apiKey));
        } catch (TransportExceptionInterface $e) {
            throw $this->badRequestException($e->getMessage());
        }

        try {
            $movie = new Movie();
            $responseArray = $response->toArray();
            $movie->setTitle($responseArray['Title']);
            $movie->setCountry(substr($responseArray['Country'], 0, 2));
            $movie->setDirector($responseArray['Director']);
            $movie->setReleasedAt(new \DateTime($responseArray['Released']));
            $movie->setSynopsis("Waaaw je la trouve pas sur IMDb?!?!");
        } catch (ClientExceptionInterface $e) {
        } catch (DecodingExceptionInterface $e) {
        } catch (RedirectionExceptionInterface $e) {
        } catch (ServerExceptionInterface $e) {
        } catch (TransportExceptionInterface $e) {
        } catch (\Exception $e) {
            throw $this->badRequestException($e->getMessage());
        }

        $this->persistFlush($movie);

        return $this->resourceCreateResponse($movie);
    }

    /**
     * Edit a movie
     *
     * @param Request $request
     * @param FormHandler $formHandler
     * @return Response
     */
    public function edit(Request $request, FormHandler $formHandler)
    {
        /**
         * @var Movie $movie
         */
        $movie = $this->em->getRepository(Movie::class)
            ->find($request->get('id'));

        if (!$movie) {
            throw $this->notFoundException();
        }

        try {
            $form = $this->getForm(MovieType::class, $movie);
            if (!$formHandler->process($form)) {
                throw $this->badRequestException($formHandler->error());
            }

            $this->persistFlush($movie);

            $this->persistFlush($movie);
        } catch (\Exception $e) {
            throw $this->badRequestException($e->getMessage());
        }

        return $this->handleView($this->view($movie));
    }

    /**
     * Delete a movie
     *
     * @param Request $request
     * @return Response
     */
    public function delete(Request $request): Response
    {
        $movie = $this->em->getRepository(Movie::class)
            ->find($request->get('id'));

        if (!$movie) {
            throw $this->notFoundException();
        }

        $id = $movie->getId();
        $title = $movie->getTitle();

        $this->persistFlush($movie);

        return $this->handleView(
            $this->view(['Remove Movie ' . $id . ' (' . $title . ') succesfully'])
        );
    }
}